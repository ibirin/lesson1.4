FROM debian:9 as build

RUN apt update && apt install -y wget gcc make libpcre++ zlib1g-dev
RUN wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20210510.tar.gz && tar zxf v2.1-20210510.tar.gz && cd luajit2-2.1-20210510 && make && make install && cd ..
RUN wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.21.tar.gz && tar zxf v0.1.21.tar.gz && cd lua-resty-core-0.1.21 && make install PREFIX=/usr/local/ && cd ..
RUN wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.10.tar.gz && tar zxf v0.10.tar.gz && cd lua-resty-lrucache-0.10 && make install PREFIX=/usr/local/ && cd ..
RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz && tar zxf v0.3.1.tar.gz
RUN wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.19.tar.gz && tar zxf v0.10.19.tar.gz
RUN wget http://nginx.org/download/nginx-1.19.9.tar.gz && tar zxf nginx-1.19.9.tar.gz && cd ./nginx-1.19.9 && LUAJIT_INC=/usr/local/include/luajit-2.1 LUAJIT_LIB=/usr/local/lib/ ./configure --with-ld-opt="-Wl,-rpath,/usr/local/lib/" --add-module=/lua-nginx-module-0.10.19 --add-module=/ngx_devel_kit-0.3.1 && make && make install

FROM debian:9
WORKDIR /usr/local/lib/
RUN mkdir -p ./lua/resty/core ./lua/resty/lrucache
COPY --from=build /usr/local/lib/lua/resty/* ./lua/resty/
COPY --from=build /usr/local/lib/lua/resty/core/* ./lua/resty/core/
COPY --from=build /usr/local/lib/lua/resty/lrucache/* ./lua/resty/lrucache/
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf ../html && touch ../logs/error.log && chmod +x nginx
COPY --from=build /usr/local/nginx/conf/mime.types ../conf/
COPY --from=build /usr/local/nginx/html/index.html ../html/
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib/libluajit-5.1.so.2
CMD ["./nginx", "-g", "daemon off;"]
